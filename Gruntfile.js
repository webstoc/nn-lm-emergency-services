/**
 * Generic form of Gruntfile - moves task and configuration
 * definition to the grunt_tasks and grunt_tasks/options
 * folders, respectively for organization.
 */
module.exports = function(grunt) {
	'use strict';
	var path = require('path');
	var timestamp = require('monotonic-timestamp');
	var pkg = grunt.file.readJSON('package.json');
	var project = pkg.name;

	require('load-grunt-config')(grunt, {
		configPath: path.join(process.cwd(), 'tasks'), //path to task.js files, defaults to grunt dir
		init: true, //auto grunt.initConfig
		loadGruntTasks: { //can optionally pass options to load-grunt-tasks.  If you set to false, it will disable auto loading tasks.
			pattern: 'grunt-*',
			config: require('./package.json'),
			scope: 'devDependencies'
		}
	});
};