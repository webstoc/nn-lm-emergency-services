module.exports = function(grunt) {
	grunt.registerTask("deploy", [
		'build',
		'copy:localhost'
	]);
	grunt.registerTask("sandy-dev", [
		'build',
		'rsync:dev'
	]);
	grunt.registerTask("sandy-stage", [
		'build',
		'rsync:stage'
	]);
};